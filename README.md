# Biallelic Inactivation #

R script that detects biallelically inactivated genes by integrating SCNA (somatic copy number alterations), germline and somatic SV (stractural variants) and germline and somatic SNV (single nucleotide variants) data.


### Requirements ###

* R (3.2.0 or newer)
* Bioconductor

To **install**, clone the repository to a local folder.

```bash
git clone git@bitbucket.org:weischenfeldt/biallelic_inactivation.git
```

### Run ###

```bash
RScript biallelic_inactivation --help
```

```bash
Rscript biallelic_inactivation.R --scna /path/to/scna/dir \
                                 --sv /path/to/sv/dir \
                                 --snv /path/to/snv/file \
                                 --scnaAnnot /path/to/scna.annotation/dir \
                                 --germsv /path/to/germline/sv/dir \
                                 --germsnv /path/to/germline/snv/file \ 
                                 -p 8 \
                                 -o results
```