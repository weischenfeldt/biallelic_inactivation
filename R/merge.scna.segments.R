merge.scnas <- function(rows, scna) {
  scna[rows[1], "end"] <- scna[rows[2], "end"]
  scna[rows[1], ]
}

vec.to.breaks <- function(i, vec, breaks) {
  vec <- vec[(breaks[i] + 1):breaks[i+1]]
  c(vec[1], vec[length(vec)] + 1)
}

merge.same.cn.segments <- function(cn, scna) {

  same_cn <- which(scna$major_cn == cn)

  keep <- c()

  if (length(same_cn) >= 2) {
    vec <- which((scna$start[same_cn[-1]] -
                    scna$end[same_cn[-length(same_cn)]]) < 2)

    if (length(vec) > 0) {
      breaks <- c(0, which(diff(vec) != 1), length(vec))
      breaks <- lapply(seq(length(breaks) - 1), vec.to.breaks, vec, breaks)

      exclude <- unlist(lapply(breaks, function(i) seq(i[1], i[2])))
      keep <- rbind(scna[same_cn[- exclude], ],
                    do.call(rbind, lapply(breaks, merge.scnas, scna[same_cn, ])))
    } else
      keep <- rbind(keep, scna[same_cn, ])

  } else {
    keep <- scna[same_cn, ]
  }

  keep
}

merge.segments <- function(chr, scna) {
  same_chr <- scna %>% filter(chromosome == chr)
  unique_cn <- unique(same_chr$major_cn)

  do.call(rbind, lapply(unique_cn, merge.same.cn.segments, same_chr))
}

# merged <- lapply(unique(scna0$chromosome), merge.segments, scna0) %>%
#   do.call(what = rbind) %>% arrange(chromosome, start)
#
#
# plot(range(cur_scna$start, cur_scna$end), range(cur_scna$major_cn),type="n")
#
# options(scipen = 1)
#
# plot(range(1,10e06), range(cur_scna$major_cn),type="n", ylab = "Major CN", xlab = "Chromosomal coordinates (bp)", main = "Unmerged")
# segments(cur_scna$start, cur_scna$major_cn, cur_scna$end, cur_scna$major_cn, lwd = 5, col = palette_pander(8))
# grid(lwd = .5)
#
# plot(range(1,10e06), range(cur_scna$major_cn),type="n", ylab = "Major CN", xlab = "Chromosomal coordinates (bp)", main = "Merged")
# segments(cur_merged$start, cur_merged$major_cn, cur_merged$end, cur_merged$major_cn, lwd = 5, col = palette_pander(8))
# grid(lwd = .5)
